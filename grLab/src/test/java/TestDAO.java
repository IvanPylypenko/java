import DAO.DAO;

import model.AppleTree;
import model.PearTree;
import model.Fruit;
import org.junit.*;


import java.sql.SQLException;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class TestDAO {

    @Test
    public void testFruitAll() throws SQLException {
        DAO.FruitDao fruitDao = mock(DAO.FruitDao.class);
        ArrayList<Fruit> toReturn = new ArrayList<>();
        for (int i = 0; i < 5; i++){
            Fruit f = new Fruit();
            f.setColor("red"+i);
            f.setId(i);
            toReturn.add(f);
        }
        when(fruitDao.getAll()).thenReturn(toReturn);
        ArrayList<Fruit> allFruit = fruitDao.getAll();
        Assert.assertEquals(allFruit.size(), 5);
        Assert.assertEquals(allFruit.get(0).getId(), 0);
        Assert.assertEquals(allFruit.get(3).getColor(), "red3");
        Assert.assertTrue(allFruit.get(1).isFresh());
    }

    @Test
    public void testFruitById() throws SQLException {
        DAO.FruitDao fruitDao = mock(DAO.FruitDao.class);
        Fruit f = new Fruit();
        f.setColor("red");
        f.setId(1);
        f.setFresh(false);
        when(fruitDao.getById(1)).thenReturn(f);

        Fruit fr = fruitDao.getById(1);

        Assert.assertEquals(fr.getId(), 1);
        Assert.assertEquals(fr.getColor(), "red");
        Assert.assertFalse(fr.isFresh());
    }

    @Test
    public void testAppleTreeAll() throws SQLException {
        DAO.AppleTreeDao appleTreeDao = mock(DAO.AppleTreeDao.class);
        ArrayList<AppleTree> toReturn = new ArrayList<>();
        for (int i = 0; i < 5; i++){
            AppleTree a = new AppleTree(i, "test"+i, i, i);
            toReturn.add(a);
        }
        when(appleTreeDao.getAll()).thenReturn(toReturn);

        ArrayList<AppleTree> appleTrees = appleTreeDao.getAll();

        Assert.assertEquals(appleTrees.size(), 5);
        Assert.assertEquals(appleTrees.get(0).getId(), 0);
        Assert.assertEquals(appleTrees.get(1).getAppleCount(), 1);
        Assert.assertEquals(appleTrees.get(2).getName(), "test2");
        Assert.assertEquals(appleTrees.get(3).getHeight(), 3);
    }

    @Test
    public void testAppleTreeById() throws SQLException {
        DAO.AppleTreeDao appleTreeDao = mock(DAO.AppleTreeDao.class);
        when(appleTreeDao.getById(1)).thenReturn(new AppleTree(1, "test", 10, 15));

        AppleTree a = appleTreeDao.getById(1);

        Assert.assertEquals(a.getId(),1);
        Assert.assertEquals(a.getHeight(), 10);
        Assert.assertEquals(a.getName(), "test");
        Assert.assertEquals(a.getAppleCount(), 15);
    }

    @Test
    public void testPearTreeAll() throws SQLException {
        DAO.PearTreeDao pearTreeDao = mock(DAO.PearTreeDao.class);
        ArrayList<PearTree> toReturn = new ArrayList<>();
        for (int i = 0; i < 5; i++){
            PearTree a = new PearTree(i, "test"+i, i, i);
            toReturn.add(a);
        }
        when(pearTreeDao.getAll()).thenReturn(toReturn);

        ArrayList<PearTree> pearTrees = pearTreeDao.getAll();

        Assert.assertEquals(pearTrees.size(), 5);
        Assert.assertEquals(pearTrees.get(0).getId(), 0);
        Assert.assertEquals(pearTrees.get(1).getPearCount(), 1);
        Assert.assertEquals(pearTrees.get(2).getName(), "test2");
        Assert.assertEquals(pearTrees.get(3).getHeight(), 3);
    }

    @Test
    public void testPearTreeById() throws SQLException {
        DAO.PearTreeDao pearTreeDao = mock(DAO.PearTreeDao.class);
        when(pearTreeDao.getById(1)).thenReturn(new PearTree(1, "test", 10, 15));

        PearTree p = pearTreeDao.getById(1);

        Assert.assertEquals(p.getId(),1);
        Assert.assertEquals(p.getHeight(), 10);
        Assert.assertEquals(p.getName(), "test");
        Assert.assertEquals(p.getPearCount(), 15);
    }
}
