import model.AppleTree;
import model.Fruit;
import model.FruitTree;
import model.PearTree;
import org.junit.Assert;
import org.junit.Test;

public class testTrees {
    @Test
    public void testFruit(){
        Fruit f = new Fruit();
        Assert.assertEquals("Fresh must be true",true,f.isFresh());
        f.setColor("red");
        f.setId((long) 12345121);
        f.spoiled();
        Assert.assertEquals("Color must be red", "red", f.getColor());
        Assert.assertEquals("Does not correct id", 12345121, f.getId());
        Assert.assertEquals("Fresh must be false", false, f.isFresh());
    }

    @Test
    public void testFruitTree(){
        FruitTree fTree = new FruitTree(10);

        Assert.assertEquals("", 10, fTree.getHeight());
        fTree.grow();
        Assert.assertEquals("", 11, fTree.getHeight());
    }

    @Test
    public void testAppleTree(){
        AppleTree appleTree = new AppleTree(1, "big tree", 10,0);
        Assert.assertEquals("Not correct name", "big tree", appleTree.getName());
        appleTree.addApple();
        Assert.assertEquals("Not correct name", 1, appleTree.getAppleCount());
    }

    @Test
    public void testPearTree(){
        PearTree pearTree = new PearTree(1, "big tree", 10,0);
        Assert.assertEquals("Not correct name", "big tree", pearTree.getName());
        pearTree.addPear();
        Assert.assertEquals("Not correct name", 1, pearTree.getPearCount());
    }
}
