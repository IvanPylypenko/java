import DAO.DAO;
import model.AppleTree;
import model.Fruit;
import model.PearTree;

import java.sql.SQLException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws SQLException {
        DAO dao = new DAO();
        System.out.println("===========AppleTree==========");
        DAO.AppleTreeDao AppleDAO = dao.new AppleTreeDao();
        ArrayList<AppleTree> apples = AppleDAO.getAll();
        System.out.println(apples);
        System.out.println("\n\n\n");
        AppleTree a = AppleDAO.getById(1);
        System.out.println(a.toString());
        System.out.println("\n\n\n===========RearTree==========");

        DAO.PearTreeDao PearDAO = dao.new PearTreeDao();
        ArrayList<PearTree> pears = PearDAO.getAll();
        System.out.println(pears);
        System.out.println("\n\n\n");
        PearTree p = PearDAO.getById(1);
        System.out.println(p.toString());

        System.out.println("\n\n\n===========Fruit==========");
        DAO.FruitDao fruitDao = dao.new FruitDao();
        ArrayList<Fruit> listFruit = fruitDao.getAll();
        System.out.println(listFruit);
        System.out.println("\n\n\n");
        Fruit f  = fruitDao.getById(1);
        System.out.println(f.toString());
    }
}