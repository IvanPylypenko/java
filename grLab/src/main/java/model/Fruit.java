package model;

public class Fruit {
    private long id;
    private String color;
    public boolean fresh;
    public Fruit(){
        this.fresh = true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFresh() {
        return fresh;
    }

    public void spoiled() {
        this.fresh = false;
    }

    public void setFresh(boolean fresh) {
        this.fresh = fresh;
    }

    public String toString() {
        return "id = " + id + "\ncolor - " + color + "\nfresh = " + fresh + "\n";
    }
}