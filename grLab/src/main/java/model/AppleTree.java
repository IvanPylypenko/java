package model;


public class AppleTree extends FruitTree {
    private long id;
    private String name;
    private int appleCount;
    public AppleTree(long id, String name, int h, int countApple) {
        super(h);
        this.id = id;
        this.name = name;
        this.appleCount = countApple;
    }
    public void addApple(){
        this.appleCount++;
    }
    public String getName() {
        return name;
    }
    public int getAppleCount() {
        return appleCount;
    }
    public long getId() {
        return id;
    }

    public String toString() {
        return "id = " + id + "\nname = " + name + "\nappleCount = " + appleCount + "\n";
    }
}

