package model;

public class FruitTree {
    private int height;
    public FruitTree(int h){
        height = h;
    }

    public int grow(){
        this.height++;
        return this.height;
    }

    public int getHeight() {
        return height;
    }
}