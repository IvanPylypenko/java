package model;


public class PearTree extends FruitTree {
    private long id;
    private String name;
    private int pearCount;
    public PearTree(long id, String name, int h, int countPear) {
        super(h);
        this.id = id;
        this.name = name;
        this.pearCount = countPear;
    }
    public void addPear(){
        this.pearCount++;
    }
    public String getName() {
        return name;
    }
    public int getPearCount() {
        return pearCount;
    }
    public long getId() {
        return id;
    }
    public String toString() {
        return "id = " + id + "\nname = " + name + "\npearCount = " + pearCount + "\n";
    }
}
