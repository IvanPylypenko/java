package DAO;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IDAO<T> {
    public T getById(long id) throws SQLException;
    public ArrayList<T> getAll() throws SQLException;
}
