package DAO;
import model.AppleTree;
import model.Fruit;
import model.PearTree;

import java.sql.*;
import java.util.ArrayList;

public class DAO {
    private Statement stmt = null;

    public DAO() {
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/postgres",
                            "postgres", "12345");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();


        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
    }

    public class AppleTreeDao implements IDAO<AppleTree> {

        @Override
        public AppleTree getById(long id) throws SQLException {
            AppleTree tree = null;
            ResultSet rs = stmt.executeQuery("SELECT * FROM public.\"AppleTree\" WHERE id = " + id + ";");
            while (rs.next()) {
                tree = new AppleTree(rs.getInt("id"), rs.getString("name"), 0, rs.getInt("appleCount"));

            }
            rs.close();
            return tree;
        }

        @Override
        public ArrayList<AppleTree> getAll() throws SQLException {
            ArrayList<AppleTree> allAppleTree = new ArrayList<>();
            ResultSet rs = stmt.executeQuery("SELECT * FROM public.\"AppleTree\";");
            System.out.println(rs.toString());
            while (rs.next()) {
                AppleTree a = new AppleTree(rs.getInt("id"), rs.getString("name"), 0, rs.getInt("appleCount"));
                allAppleTree.add(a);
            }
            rs.close();
            return allAppleTree;
        }
    }

    public class PearTreeDao implements IDAO<PearTree> {

        @Override
        public PearTree getById(long id) throws SQLException {
            PearTree tree = null;
            ResultSet rs = stmt.executeQuery("SELECT * FROM public.\"PearTree\" WHERE id = " + id + ";");
            while (rs.next()) {
                tree = new PearTree(rs.getInt("id"), rs.getString("name"), 0, rs.getInt("pearCount"));

            }
            rs.close();
            return tree;
        }

        @Override
        public ArrayList<PearTree> getAll() throws SQLException {
            ArrayList<PearTree> allPearTree = new ArrayList<>();
            ResultSet rs = stmt.executeQuery("SELECT * FROM public.\"PearTree\";");
            while (rs.next()) {
                PearTree p = new PearTree(rs.getInt("id"), rs.getString("name"), 0, rs.getInt("pearCount"));
                allPearTree.add(p);
            }
            rs.close();
            return allPearTree;
        }
    }

    public class FruitDao implements IDAO<Fruit> {
        @Override
        public Fruit getById(long id) throws SQLException {
            Fruit f = null;
            ResultSet rs = stmt.executeQuery("SELECT * FROM public.\"Fruit\" WHERE id = " + id + ";");
            while (rs.next()) {
                f = new Fruit();
                f.setId(rs.getInt("id"));
                f.setColor(rs.getString("color"));
                f.setFresh(rs.getBoolean("fresh"));
            }
            rs.close();
            return f;
        }

        @Override
        public ArrayList<Fruit> getAll() throws SQLException {
            ArrayList<Fruit> allFruit = new ArrayList<>();
            ResultSet rs = stmt.executeQuery("SELECT * FROM public.\"Fruit\";");
            while (rs.next()) {
                Fruit f = new Fruit();
                f.setId(rs.getInt("id"));
                f.setColor(rs.getString("color"));
                f.setFresh(rs.getBoolean("fresh"));
                allFruit.add(f);
            }
            rs.close();
            return allFruit;
        }
    }
}